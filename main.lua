--[[
    Author: Kris Laermans
    kris@laermans.be

    Based on the work of :
    GD50 2018
    Pong Remake

    -- Main Program --

    Author for base pong app : Colton Ogden
    cogden@cs50.harvard.edu

    Originally programmed by Atari in 1972. Features two
    paddles, controlled by players, with the goal of getting
    the ball past your opponent's edge. First to 10 points wins.

    This version is built to more closely resemble the NES than
    the original Pong machines or the Atari 2600 in terms of
    resolution, though in widescreen (16:9) so it looks nicer on 
    modern systems.
]]

-- push is a library that will allow us to draw our game at a virtual
-- resolution, instead of however large our window is; used to provide
-- a more retro aesthetic
--
-- https://github.com/Ulydev/push
push = require 'push'

-- the "Class" library we're using will allow us to represent anything in
-- our game as code, rather than keeping track of many disparate variables and
-- methods
--
-- https://github.com/vrld/hump/blob/master/class.lua
Class = require 'class'

-- our Paddle class, which stores position and dimensions for each Paddle
-- and the logic for rendering them
require 'Paddle'
require 'Tractor'
-- our Ball class, which isn't much different than a Paddle structure-wise
-- but which will mechanically function very differently
require 'Ball'

-- size of our actual window
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 640

-- size we're trying to emulate with push
VIRTUAL_WIDTH = 800
VIRTUAL_HEIGHT = 640

-- paddle movement speed
PADDLE_SPEED = 200
PADDLE_HEIGHT = 70

K_COEFF = 600
DAMPER_COEFF = 300
MAX_TRACTORS = 3
COLLIDING_ZONE = 10
--[[
    Called just once at the beginning of the game; used to set up
    game objects, variables, etc. and prepare the game world.
]]
function love.load()
    -- set love's default filter to "nearest-neighbor", which essentially
    -- means there will be no filtering of pixels (blurriness), which is
    -- important for a nice crisp, 2D look
    love.graphics.setDefaultFilter('nearest', 'nearest')

    -- set the title of our application window
    love.window.setTitle('Pong')

    -- seed the RNG so that calls to random are always random
    math.randomseed(os.time())

    -- initialize our nice-looking retro text fonts
    smallFont = love.graphics.newFont('font.ttf', 8)
    largeFont = love.graphics.newFont('font.ttf', 16)
    scoreFont = love.graphics.newFont('font.ttf', 32)
    love.graphics.setFont(smallFont)

    previousBallXSpeed = 0
    previousBallYSpeed = 0

    -- set up our sound effects; later, we can just index this table and
    -- call each entry's `play` method
    sounds = {
        ['paddle_hit'] = love.audio.newSource('sounds/paddle_hit.wav', 'static'),
        ['score'] = love.audio.newSource('sounds/score.wav', 'static'),
        ['wall_hit'] = love.audio.newSource('sounds/wall_hit.wav', 'static')
    }
    
    -- initialize our virtual resolution, which will be rendered within our
    -- actual window no matter its dimensions
    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        resizable = false,
        vsync = true,
        canvas = false
    })

    -- initialize our player paddles; make them global so that they can be
    -- detected by other functions and modules
    player1 = Paddle(10, WINDOW_HEIGHT/2-PADDLE_HEIGHT/2, 10, PADDLE_HEIGHT)
    
    -- place a ball in the middle of the screen
    ball = Ball(VIRTUAL_WIDTH / 2 - 2, VIRTUAL_HEIGHT / 2 - 2, 10, 10)
    
    --tractor = Tractor(10, 10, 4, 4, PADDLE_HEIGHT)
    tractors = {}
    table.insert(tractors, Tractor(90, WINDOW_HEIGHT/2, 4, 4, PADDLE_HEIGHT))
    table.insert(tractors, Tractor(190, WINDOW_HEIGHT/3, 4, 4, PADDLE_HEIGHT))
    table.insert(tractors, Tractor(250, 2*WINDOW_HEIGHT/3, 4, 4, PADDLE_HEIGHT))

    
    -- initialize score variables
    player1Score = 0
    player1PreviousScore = 0
    player1BestScore = 0
    paddleCollideCount = 0
    -- either going to be 1 or 2; whomever is scored on gets to serve the
    -- following turn
    servingPlayer = 1
    time_elapsed = 0
    start_time = 0
    previous_time_elapsed = 0
    -- player who won the game; not set to a proper value until we reach
    -- that state in the game
    winningPlayer = 0
    tryToPauseBehindLimits = false
    ropeDecreaseScore = 1

    
    -- the state of our game; can be any of the following:
    -- 1. 'start' (the beginning of the game, before first serve)
    -- 2. 'serve' (waiting on a key press to serve the ball)
    -- 3. 'play' (the ball is in play, bouncing between paddles)
    -- 4. 'done' (the game is over, with a victor, ready for restart)
    --player1BestScore = line_from('score.dat')
    gameState = 'start'
end

--[[function file_exists(file)
    local f = io.open(file, "rb")
    if f then f:close() end
    return f ~= nil
end

function line_from(file)
    if not file_exists(file) then return 0 end
    lines = io.lines(file)
    if lines[1] ~= nil then
        return lines[1]    
    end
    return 0
end
]]--
--[[
    Called whenever we change the dimensions of our window, as by dragging
    out its bottom corner, for example. In this case, we only need to worry
    about calling out to `push` to handle the resizing. Takes in a `w` and
    `h` variable representing width and height, respectively.
]]
function love.resize(w, h)
    print(w, " ", h)
    VIRTUAL_WIDTH = w
    VIRTUAL_HEIGHT = h
    push:resize(w, h)
end

function getLimitScreen()
    return WINDOW_WIDTH*(1/4 + (3/4)*paddleCollideCount/40)
end

--[[
    Called every frame, passing in `dt` since the last frame. `dt`
    is short for `deltaTime` and is measured in seconds. Multiplying
    this by any changes we wish to make in our game will allow our
    game to perform consistently across all hardware; otherwise, any
    changes we make will be applied as fast as possible and will vary
    across system hardware.
]]
function tractorsCollide(tractors, x, y)
    
    notColliding = true
    for key, tractor in pairs(tractors) do
        tractorX, tractorY = tractor:getPosition()
        if math.abs(x-tractorX)<COLLIDING_ZONE and math.abs(y-tractorY)<COLLIDING_ZONE then
            notColliding = false
        end
    end
    return notColliding
end

function love.mousereleased(x, y, button)
    if tractorsCollide(tractors, x-3, y-3) then
        ropeDecreaseScore = ropeDecreaseScore * (x/WINDOW_WIDTH)
        table.insert(tractors, Tractor(x-3, y-3, 4, 4, PADDLE_HEIGHT))
    end
end

function love.update(dt)
    player1.dy = 0
    i = 1
    
    for key, tractor in pairs(tractors) do
        tractorX, tractorY = tractor:getPosition()
        -- Calculate attaction of the tractors
        force = (K_COEFF * (1/tractorX) * (tractorY-player1.y-(PADDLE_HEIGHT/2)))
        if math.abs(force)>1000 then
            force = 1000*force/math.abs(force)
        end
        player1.dy = player1.dy/2 + force
        tractorSize = #tractors
        if tractorSize >= 15 then
            table.remove(tractors, 1)
        end
    end
    if gameState == 'pause' then
        
        if start_time ~= 0 then
            time_elapsed = previous_time_elapsed + os.difftime(os.time()-start_time)
            if time_elapsed > 5 then
                time_elapsed = 5
            end    
        end
        if (previousBallXSpeed > 0 or ball.x > getLimitScreen()) and time_elapsed < 5 then
            if ball.dx ~= 0 then
                previousBallXSpeed = ball.dx
                previousBallYSpeed = ball.dy
            end
            ball.dx = 0
            ball.dy = 0
        else 
            --start_time = 0
            --time_elapsed = 0
            tryToPauseBehindLimits = true
            gameState = "play"
        end
    elseif gameState == 'serve' then
        -- before switching to play, initialize ball's velocity based
        -- on player who last scored
        ball.dy = math.random(-50, 50)
        ball.dx = 140
        previousBallXSpeed = 0
        previousBallYSpeed = 0
        

        
    elseif gameState == 'play' then
        if previousBallXSpeed ~= 0 then
            ball.dx = previousBallXSpeed
            ball.dy = previousBallYSpeed
        end
        -- detect ball collision with paddles, reversing dx if true and
        -- slightly increasing it, then altering the dy based on the position
        -- at which it collided, then playing a sound effect
        if ball:collides(player1) then
            ball.dx = -ball.dx * 1.03
            ball.x = player1.x + 10

            -- keep velocity going in the same direction, but randomize it
            if ball.dy < 0 then
                ball.dy = -math.random(10, 50)
            else
                ball.dy = math.random(10, 50)
            end
            start_time = 0
            time_elapsed = 0
            tryToPauseBehindLimits = false
            player1Score = player1Score + math.floor(100 * ropeDecreaseScore)
            ropeDecreaseScore = 1
            paddleCollideCount = paddleCollideCount + 1
            sounds['paddle_hit']:play()
        end
        

        -- detect upper and lower screen boundary collision, playing a sound
        -- effect and reversing dy if true
        if ball.y <= 0 then
            ball.y = 0
            ball.dy = -ball.dy
            sounds['wall_hit']:play()
        end

        -- -4 to account for the ball's size
        if ball.y >= VIRTUAL_HEIGHT - 4 then
            ball.y = VIRTUAL_HEIGHT - 4
            ball.dy = -ball.dy
            sounds['wall_hit']:play()
        end

        -- if we reach the left or right edge of the screen, go back to serve
        -- and update the score and serving player
        if ball.x < 0 then
            servingPlayer = 1
            sounds['score']:play()
            gameState = 'serve'
            start_time = 0
            time_elapsed = 0
            player1PreviousScore = player1Score
            player1Score = 0
            -- tractors = {}
            -- tryToPauseBehindLimits = false
            -- ropeDecreaseScore = 1
            -- paddleCollideCount = 0
            --     -- places the ball in the middle of the screen, no velocity
            -- player1:reset()
            -- ball:reset(player1)
        end

        if ball.x > VIRTUAL_WIDTH then
            ball.x = VIRTUAL_WIDTH - 4
            ball.dx = -ball.dx
            sounds['wall_hit']:play()
        end
        if ball.dx ~= 0 then
            previousBallXSpeed = ball.dx
            previousBallYSpeed = ball.dy
        end
    end

 
    -- update our ball based on its DX and DY only if we're in play state;
    -- scale the velocity by dt so movement is framerate-independent
    if gameState == 'play' then
        ball:update(dt)
    end

    player1:update(dt)
end

--[[
    A callback that processes key strokes as they happen, just the once.
    Does not account for keys that are held down, which is handled by a
    separate function (`love.keyboard.isDown`). Useful for when we want
    things to happen right away, just once, like when we want to quit.
]]
function love.keypressed(key)
    -- `key` will be whatever key this callback detected as pressed
    if key == 'escape' then
        -- the function LÖVE2D uses to quit the application
        love.event.quit()
    -- if we press enter during either the start or serve phase, it should
    -- transition to the next appropriate state
    elseif key == 'enter' or key == 'return' then
        if gameState == 'start' then
            tractors = {}
            gameState = 'serve'
        elseif gameState == 'serve' then
            gameState = 'play'
            tractors = {}
            previous_time_elapsed = 0
            tryToPauseBehindLimits = false
            ropeDecreaseScore = 1
            paddleCollideCount = 0
                -- places the ball in the middle of the screen, no velocity
            player1:reset()
            ball:reset(player1)
            ball.dy = math.random(-50, 50)
            ball.dx = 140
        elseif gameState == 'done' then
            -- game is simply in a restart phase here, but will set the serving
            -- player to the opponent of whomever won for fairness!
            --tractors = {}
            gameState = 'serve'

            ball:reset(player1)
            previous_time_elapsed = 0
            -- reset scores to 0
            player1Score = 0
            -- decide serving player as the opposite of who won
            servingPlayer = 1
        end
    elseif key == 'space' and gameState ~= 'serve' and gameState ~= 'start' then
        if gameState == 'pause' then
            gameState = 'play'
            previous_time_elapsed = time_elapsed
            start_time = 0
        else 
            gameState = 'pause'
            start_time = os.time()
            previous_time_elapsed = time_elapsed
        end
    end
end


--[[
    Called each frame after update; is responsible simply for
    drawing all of our game objects and more to the screen.
]]
function love.draw()
    -- begin drawing with push, in our virtual resolution
    
    push:start()
    love.graphics.setBackgroundColor(40, 45, 52)
    love.graphics.setColor(0.4, 0.3, 0.3, 100)
    if (previousBallXSpeed < 0 and ball.x < getLimitScreen() and tryToPauseBehindLimits) then
        love.graphics.setColor(0.2, 0, 0, 100)
    end
    love.graphics.rectangle('fill', 0, 0, getLimitScreen(), WINDOW_HEIGHT)
    love.graphics.setColor(1, 1, 1, 255)
    love.graphics.rectangle('fill', 0, 0, WINDOW_WIDTH, 5)
    love.graphics.rectangle('fill', WINDOW_WIDTH-5, 0, WINDOW_WIDTH, WINDOW_HEIGHT)
    love.graphics.rectangle('fill', 0, WINDOW_HEIGHT-5, WINDOW_WIDTH, WINDOW_HEIGHT)
    love.graphics.setColor(0, 255, 255, 255)
    
    -- render different things depending on which part of the game we're in
    if gameState == 'start' then
        -- UI messages
        love.graphics.setFont(largeFont)
        love.graphics.printf('Welcome to Pong!', 0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Press Enter to begin!', 0, 30, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('The top, bottom and right side are walls where the ball will bounce', 0, 190, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('With a rope, you can move the paddle', 0, 210, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('A rope attached on the right side is less attractive for the paddle', 0, 230, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('More the rope is attached to the right, more you earn points', 0, 250, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('You can freeze (space key) the ball for 5 seconds, unless you are in the grey zone', 0, 270, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Each time you touch the ball, you win a point', 0, 290, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Try to attach new ropes !', 0, 350, VIRTUAL_WIDTH, 'center')
        love.graphics.printf("More you have ropes, more it's difficult", 0, 370, VIRTUAL_WIDTH, 'center')
        ball:reset(player1)
    elseif gameState == 'serve' then
        -- UI messages
        love.graphics.setFont(largeFont)
        love.graphics.printf('Press Enter to serve!', 0, 20, VIRTUAL_WIDTH, 'center')
        if player1PreviousScore ~= 0 then
            love.graphics.printf('Score : ' .. player1PreviousScore, 0, WINDOW_HEIGHT/2, VIRTUAL_WIDTH, 'center')
        end
    elseif gameState == 'play' then
        -- no UI messages to display in play
    elseif gameState == 'done' then
        -- UI messages
        love.graphics.setFont(largeFont)
        love.graphics.printf('Player ' .. tostring(winningPlayer) .. ' wins!',
            0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.setFont(smallFont)
        love.graphics.printf('Press Enter to restart!', 0, 30, VIRTUAL_WIDTH, 'center')
    end

    -- show the score before ball is rendered so it can move over the text
    displayScore()
    
    player1:render()
    --player2:render()
    ball:render()
    for key, tractor in pairs(tractors) do
        tractor:render(player1)
    end
    -- display FPS for debugging; simply comment out to remove
    displayFPS()

    -- end our drawing to push
    push:finish()
end

--[[
    Simple function for rendering the scores.
]]
function displayScore()
    -- score display
    love.graphics.setFont(scoreFont)
    time_left = 5 - time_elapsed
    colorR = 255 * (5 - time_left)
    colorG = 255 * (time_left)/5
    love.graphics.setColor(colorR, colorG, 0, 100)
    love.graphics.rectangle('fill', VIRTUAL_WIDTH * 0.5 - 7, VIRTUAL_HEIGHT / 10 - 5, 30, 38)
    love.graphics.setColor(0.1, 0.1, 0.1, 100)
    love.graphics.print(tostring(time_left), VIRTUAL_WIDTH * 0.5,
        VIRTUAL_HEIGHT / 10)
    love.graphics.setColor(0, 255, 255, 255)
    if player1PreviousScore ~= 0 then
        love.graphics.setFont(largeFont)
        if player1PreviousScore > player1BestScore then
            player1BestScore = player1PreviousScore
        end
        love.graphics.print('Best score ' .. tostring(player1BestScore), -40 + VIRTUAL_WIDTH * 0.8, -30 + VIRTUAL_HEIGHT / 10)
        love.graphics.setFont(scoreFont)
    end
    love.graphics.print(tostring(player1Score), VIRTUAL_WIDTH * 0.8,
        VIRTUAL_HEIGHT / 10)
end

--[[
    Renders the current FPS.
]]
function displayFPS()
    -- simple FPS display across all states
    love.graphics.setFont(smallFont)
    love.graphics.setColor(0, 255, 0, 255)
    love.graphics.print('FPS: ' .. tostring(love.timer.getFPS()), 10, 10)
    love.graphics.setColor(0, 255, 255, 255)
end
