--[[
    Author: Kris Laermans
    kris@laermans.be

    Based on the work of :
    GD50 2018
    Pong Remake

    -- Ball Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    Represents a ball which will bounce back and forth between paddles
    and walls until it passes a left or right boundary of the screen,
    scoring a point for the opponent.
]]

Tractor = Class{}

function Tractor:init(x, y, width, height, paddle_height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.paddle_height = paddle_height

    -- these variables are for keeping track of our velocity on both the
    -- X and Y axis, since the ball can move in two dimensions
    self.dy = 0
    self.dx = 0
end

--[[
    Expects a paddle as an argument and returns true or false, depending
    on whether their rectangles overlap.
]]
function Tractor:collides(tractors)
    -- first, check to see if the left edge of either is farther to the right
    -- than the right edge of the other
    if self.x > paddle.x + paddle.width or paddle.x > self.x + self.width then
        return false
    end

    -- then check to see if the bottom edge of either is higher than the top
    -- edge of the other
    if self.y > paddle.y + paddle.height or paddle.y > self.y + self.height then
        return false
    end 

    -- if the above aren't true, they're overlapping
    return true
end

--[[
    Places the ball in the middle of the screen, with no movement.
]]
function Tractor:reset()
    self.x = VIRTUAL_WIDTH / 2 - 2
    self.y = VIRTUAL_HEIGHT / 2 - 2
    self.dx = 0
    self.dy = 0
end

function Tractor:update(x, y)
    self.x = x
    self.y = y
end

function Tractor:getPosition()
    return self.x, self.y
end

function Tractor:render(paddle)
    love.graphics.setColor(1, 1, 0, 50)
    love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
    love.graphics.line(self.x+1, self.y+1, paddle.x+5, paddle.y+self.paddle_height/2)
    love.graphics.setColor(0, 255, 255, 255)
end